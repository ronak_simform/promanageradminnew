/**=========================================================
 * Module: config.js
 * App routes and resources configuration
 =========================================================*/


(function() {
    'use strict';

    angular
        .module('app.routes')
        .config(routesConfig);

    routesConfig.$inject = ['$stateProvider', '$locationProvider', '$urlRouterProvider', 'RouteHelpersProvider'];
    function routesConfig($stateProvider, $locationProvider, $urlRouterProvider, helper){

        // Set the following to true to enable the HTML5 Mode
        // You may have to set <base> tag in index and a routing configuration in your server
        $locationProvider.html5Mode(false);

        // defaults to dashboard
        

        //
        // Application Routes
        // -----------------------------------
        $stateProvider
          .state('app', {
              url: '/app',
              abstract: true,
              templateUrl: helper.basepath('app.html'),
              resolve: helper.resolveFor('fastclick', 'modernizr', 'icons', 'screenfull', 'animo', 'sparklines', 'slimscroll', 'classyloader', 'toaster', 'whirl','ngDialog','ngTable')
          })
          .state('app.dashboarduser', {
              url: '/dashboard',
              title: 'DashboardUser',
              controller: 'UserController',
              templateUrl: helper.basepath('DashboardUser.html'),
              resolve: helper.resolveFor('flot-chart','flot-chart-plugins', 'weather-icons'),
              access: { allowAnonymous: true },
              data : { pageTitle: 'User Dashboard' }
          })
          .state('app.userlist', {
              url: '/Dashboard/UserList',
              title: 'UserList',
              controller: 'UserController',
              templateUrl: helper.basepath('UserList.html'),
              access: { allowAnonymous: true },
              data : { pageTitle: 'User List' }
          })
          .state('app.dashboardinvestor', {
              url: '/dashboard/Investor',
              title: 'DashboardInvestor',
              controller: 'InvestorController',
              templateUrl: helper.basepath('DashboardInvestor.html'),
              resolve: helper.resolveFor('flot-chart','flot-chart-plugins', 'vector-map', 'vector-map-maps'),
              access: { allowAnonymous: false },
              data : { pageTitle: 'Investor Dashboard' }
          })
          .state('app.investorlist', {
              url: '/Dashboard/InvestorList',
              title: 'InvestorList',
              controller: 'InvestorController',
              templateUrl: helper.basepath('InvestorList.html'),
              access: { allowAnonymous: false },
              data : { pageTitle: 'Investor List' }
          })
          .state('app.waitinglist', {
              url: '/Dashboard/WaitingList',
              title: 'WaitingList',
              controller: 'WaitingListController',
              templateUrl: helper.basepath('WaitingList.html'),
              access: { allowAnonymous: true },
              data : { pageTitle: 'Waiting List' }
          })
          .state('app.country', {
              url: '/Manage/Country',
              title: 'Country',
              controller: 'CountryController',
              templateUrl: helper.basepath('ManageCountry.html'),
              access: { allowAnonymous: false },
              data : { pageTitle: 'Country' }
          })
          .state('app.faq', {
              url: '/Manage/FAQ',
              title: 'FAQ',
              controller: 'FAQController',
              templateUrl: helper.basepath('ManageFAQ.html'),
              access: { allowAnonymous: false },
              data : { pageTitle: 'FAQ' }
          })
          .state('app.ageverification', {
              url: '/Manage/AgeVerification',
              title: 'AgeVerification',
              controller: 'AgeVerificationController',
              templateUrl: helper.basepath('ManageAgeVerification.html'),
              access: { allowAnonymous: false },
              data : { pageTitle: 'Age Verification' }
          })
          .state('app.socialsite', {
              url: '/Manage/SocialSite',
              title: 'SocialSite',
              controller: 'SocialSiteController',
              templateUrl: helper.basepath('ManageSocialSite.html'),
              access: { allowAnonymous: false },
              data : { pageTitle: 'Social Connection' }
          })
          .state('app.userchannel', {
              url: '/Manage/UserChannel',
              title: 'UserChannel',
              controller: 'ChannelController',
              templateUrl: helper.basepath('ManageUserChannel.html'),
              access: { allowAnonymous: false },
              data : { pageTitle: 'User Channel' }
          })
          .state('app.favourite', {
              url: '/Manage/FavouriteList',
              title: 'FavouriteList',
              controller: 'FavouriteListController',
              templateUrl: helper.basepath('ManageFavouriteList.html'),
              access: { allowAnonymous: false },
              data : { pageTitle: 'Favourite List' }
          })
           .state('app.placeholder', {
              url: '/Manage/Placeholder',
              title: 'Placeholder',
              controller: 'PlaceholderController',
              templateUrl: helper.basepath('ManagePlaceholder.html'),
              resolve: helper.resolveFor('angularFileUpload', 'filestyle'),
              access: { allowAnonymous: false },
              data : { pageTitle: 'Placeholder' }
          })
          .state('app.category', {
              url: '/Manage/Category',
              title: 'Category',
              controller: 'CategoryController',
              templateUrl: helper.basepath('ManageCategory.html'),
              access: { allowAnonymous: false },
              data : { pageTitle: 'Category' }
          })
          .state('app.preregistration', {
              url: '/Manage/PreRegistration',
              title: 'PreRegistration',
              controller: 'PreRegistrationController',
              templateUrl: helper.basepath('ManagePreRegistration.html'),
              resolve: helper.resolveFor('angularFileUpload', 'filestyle'),
              access: { allowAnonymous: false },
              data : { pageTitle: 'Pre-Registration' }
          })
          .state('app.landing', {
              url: '/Manage/Landing',
              title: 'Landing',
              controller: 'LandingController',
              templateUrl: helper.basepath('ManageLandingDetail.html'),
              resolve: helper.resolveFor('angularFileUpload', 'filestyle'),
              access: { allowAnonymous: false },
              data : { pageTitle: 'Landing' }
          })
          .state('page', {
              url: '/page',
              templateUrl: 'app/pages/page.html',
              resolve: helper.resolveFor('modernizr', 'icons','toaster'),
              access: { allowAnonymous: false }
          })
           .state('page.login', {
              url: '/login',
              title: 'Login',
              controller: 'LoginFormController',
              templateUrl: 'app/pages/login.html',
              access: { allowAnonymous: true },
              data : { pageTitle: 'Login' }
          })
          .state('page.recover', {
              url: '/recover',
              title: 'Recover',
              controller: 'ForgottController',
              templateUrl: 'app/pages/recover.html',
              access: { allowAnonymous: true },
              data : { pageTitle: 'Forgot Password' }
          })
          .state('page.404', {
              url: '/404',
              title: 'Not Found',
              templateUrl: 'app/pages/404.html',
              access: { allowAnonymous: true },
              data : { pageTitle: '404' }
          })
          $urlRouterProvider.otherwise('/app/dashboard');
          ;

    } // routesConfig

})();

