/**=========================================================
 * Module: sidebar-menu.js
 * Handle sidebar collapsible elements
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.sidebar')
        .controller('TopbarController', TopbarController);

    TopbarController.$inject = ['$rootScope', '$scope', '$state', 'SidebarLoader', 'Utils','authService','$cookies'];
    function TopbarController($rootScope, $scope, $state, SidebarLoader,  Utils,authService,$cookies) {
        
        activate();

        ////////////////

        function activate() {
          var collapseList = [];

           if($cookies.getObject("authorizationData"))
          {
            $scope.username = $cookies.getObject("authorizationData").userName;
          }


        } // activate
    }

})();
