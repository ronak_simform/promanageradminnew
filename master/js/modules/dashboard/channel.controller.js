(function() {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('ChannelController', ChannelController);
        
    ChannelController.$inject = ['$rootScope', '$scope', '$state','$http','toaster','rootUrl'];
    function ChannelController($rootScope, $scope, $state,$http,toaster,rootUrl) {
            $scope.channelgrid = false;
            
            $scope.getStatus = function () {
                $http({
                    method: 'get',
                    url: rootUrl + '/api/admin/getConfigurations',
                    headers: { 'content-type': 'application/json'},
                })
                .success(function (data, status, headers, config) {
                     if (data.status == "success") {
                       $scope.channelgrid = true;
                       $scope.channelStatus = data.result.status;
                      }
                      else {
                        $scope.channelgrid = false;
                        toaster.pop("error", "Channel", "Error while fetching channel status");
                        }
                })
                .error(function () {
                    toaster.pop("error", "Channel", "Error while fetching channel status");
                });
            }

            $scope.enablebtn = function (status) {
                
                var objchannel = { status: status }
                $http({
                    method: 'put',
                    url: rootUrl + '/api/admin/manageCustomChannel',
                    headers: { 'content-type': 'application/json' },
                    data: objchannel
                })
                .success(function (data, status, headers, config) {
                     if (data.status == "success") {
                       $scope.getStatus();
                       toaster.pop("success", "Channel", "Channel status change successfully");
                      }
                      else {
                           toaster.pop("error", "Channel", "Error while changing status of channel");
                       }
                   }).error(function () {
                       toaster.pop("error", "Channel", "Error while changing status of channel");
                   });

            }
          
            $scope.getStatus();
    }
})();