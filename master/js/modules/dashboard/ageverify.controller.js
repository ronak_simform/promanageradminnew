(function() {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('AgeVerificationController', AgeVerificationController);
        
    AgeVerificationController.$inject = ['$rootScope', '$scope', '$state','$http','toaster','rootUrl'];
    function AgeVerificationController($rootScope, $scope, $state,$http,toaster,rootUrl) {
            $scope.tableage = false;
            $scope.minage = null;
            $scope.maxage = null;
            $scope.fromList = [
              '1', '2', '3', '4', '5',
              '6', '7', '8', '9', '10',
              '11', '12', '13', '14', '15',
              '16', '17', '18', '19', '20',
              '21', '22', '23', '24', '25',
              '26', '27', '28', '29', '30',
              '31', '32', '33', '34', '35',
              '36', '37', '38', '39', '40',
              '41', '42', '43', '44', '45',
              '46', '47', '48', '49', '50',
              '51', '52', '53', '54', '55',
              '56', '57', '58', '59', '60',
              '61', '62', '63', '64', '65',
              '66', '67', '68', '69', '70',
              '71', '72', '73', '74', '75',
              '76', '77', '78', '79', '80',
              '81', '82', '83', '84', '85',
              '86', '87', '88', '89', '90',
              '91', '92', '93', '94', '95',
              '96', '97', '98', '99', '100'
            ]
            $scope.toList = [
                '1', '2', '3', '4', '5',
              '6', '7', '8', '9', '10',
              '11', '12', '13', '14', '15',
              '16', '17', '18', '19', '20',
              '21', '22', '23', '24', '25',
              '26', '27', '28', '29', '30',
              '31', '32', '33', '34', '35',
              '36', '37', '38', '39', '40',
              '41', '42', '43', '44', '45',
              '46', '47', '48', '49', '50',
              '51', '52', '53', '54', '55',
              '56', '57', '58', '59', '60',
              '61', '62', '63', '64', '65',
              '66', '67', '68', '69', '70',
              '71', '72', '73', '74', '75',
              '76', '77', '78', '79', '80',
              '81', '82', '83', '84', '85',
              '86', '87', '88', '89', '90',
              '91', '92', '93', '94', '95',
              '96', '97', '98', '99', '100'
            ]

            $scope.getAgeVerify = function () {
                $http({
                    method: 'get',
                    url: rootUrl + '/api/admin/getConfigurations',
                    headers: { 'content-type': 'application/json'},
                })
                .success(function (data, status, headers, config) {
                    if (data.status == "success") {
                        $scope.tableage = true;
                        $scope.minage = data.result.min_age;
                        $scope.maxage = data.result.max_age;
                    }
                    else {
                        $scope.tableage = false;
                        toaster.pop("error", "Age Verification", "Error while fetching age verification detail");
                    }
                })
                .error(function () {
                    toaster.pop("error", "Age Verification", "Error while fetching age verification detail");
                });
            }

            $scope.submitbtn = function () {
                if ($scope.selectfrom > $scope.selectto) {
                    toaster.pop("error", "Age Verification", "Age from not grater than age to");
                }
                else {
                    var objage = { min_age: $scope.selectfrom, max_age: $scope.selectto }
                    $http({
                        method: 'put',
                        url: rootUrl + '/api/admin/manageAgeConfiguration',
                        headers: { 'content-type': 'application/json'},
                        data: objage
                    })
                    .success(function (data, status, headers, config) {
                        if (data.status == "success") {
                            toaster.pop("success", "Age Verification", "Age detail saved successfully");
                            $scope.getAgeVerify();
                        }
                        else {
                            toaster.pop("error", "Age Verification", "Error while fetching age detail");
                        }
                    }).error(function () {
                        toaster.pop("error", "Age Verification", "Error while fetching age detail");
                    });

                }
            }

            $scope.getAgeVerify();
    }
})();