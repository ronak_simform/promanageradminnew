(function() {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('LandingController', LandingController);
        
    LandingController.$inject = ['$scope','$http','toaster','fileReader','$sce','rootUrl'];
    function LandingController($scope,$http,toaster,fileReader,$sce,rootUrl) {
            
            var S3BaseUrl='https://s3-us-west-1.amazonaws.com/pro-manager/images/';
            $scope.article = {};
            $scope.Landing = {};

            $scope.getLandingcoverpic = function () {
                $http({
                    method: 'get',
                    url: rootUrl + '/api/admin/getPages',
                    headers: { 'content-type': 'application/json'},
                })
                .success(function (data, status, headers, config) {
                    if (data.status == "success") {
                        $scope.Landing = data.result;
                    }
                    else {
                        toaster.pop("error", "Landing", "Error while fetching landing detail");
                    }
                })
                .error(function () {
                    toaster.pop("error", "Landing", "Error while fetching landing detail");
                });
            }

            $scope.uploadFile = function (event) {
	            var files = event.target.files;
	            $scope.pictureFile= files[0];
	            $scope.pictureName = Date.now() + '.' + $scope.pictureFile.name.split('.').pop();
	            fileReader.readAsDataUrl($scope.pictureFile, $scope)
        			  .then(function (result) {
		        		  $scope.article.picture = result;
			        });
            };

            $scope.upload = function (Landingobject,index) {
              if($scope.pictureFile)
               {
                    $http.post(rootUrl + '/api/auth/preSignUrl', {
                        filename: $scope.pictureName,
                        contentType: $scope.pictureFile.type
                    }).success(function (resp) {
                    $http.put(resp.url, $scope.pictureFile, {headers: {'Content-Type': $scope.pictureFile.type}})
                        .success(function (resp) {
                            var objcoverimg = {};
                            $scope.article.picture = S3BaseUrl + $scope.pictureName;
                            objcoverimg = { coverPicture :  $scope.article.picture }
                            $http({
                                method: 'put',
                                url: rootUrl + '/api/admin/editPage/' + Landingobject._id,
                                headers: { 'content-type': 'application/json' },
                                data: objcoverimg
                                }).success(function (resp) {
                                    $scope.Landing[index].coverPicture = $scope.article.picture;
                                    toaster.pop("success", "Landing", "Cover picture uploaded successfully");
                                    })
                                .error(function (resp) {
                                    toaster.pop("error", "Landing", "An Error occurred while saving file");
                                });
                        })
                        .error(function (resp) {
                            toaster.pop("error", "Landing", "An Error occurred while saving file");
                        });
                    }).error(function (resp) {
                        toaster.pop("error", "Landing", "An Error occurred while saving file");
                    });
                }
                else
                {
                        toaster.pop("error", "Landing", "Please select file");
                }       
            }

            $scope.landingsubmibtn = function (newlanding,mode,index) {
                
                if(mode == "name")
                {
                    var objlanding = { name : newlanding.name }
                }
                else if(mode == "description")
                {
                    var objlanding = { description : newlanding.description }
                }
                
                $http({
                    method: 'put',
                    url: rootUrl + '/api/admin/editPage/' + newlanding._id,
                    headers: { 'content-type': 'application/json' },
                    data: objlanding
                })
                .success(function (data, status, headers, config) {
                    if (data.status == "success") {
                          if(mode == "name")
                          {
                                $scope.Landing[index].name = newlanding.name;
                                toaster.pop("success", "Landing", "Landing name saved successfully");
                          }
                          else if(mode == "description")
                          {
                                $scope.Landing[index].description = newlanding.description;
                                toaster.pop("success", "Landing", "Landing description saved successfully");
                          }
                    }
                    else {
                        toaster.pop("error", "Landing", "Error while saving Landing detail");
                    }

                }).error(function () {
                    toaster.pop("error", "Landing", "Error while saving Landing detail");
                });
            }

       $scope.getLandingcoverpic();
            
    }
   
})();