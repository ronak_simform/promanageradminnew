(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('PreRegistrationController', PreRegistrationController)

    PreRegistrationController.$inject = ['$scope', '$http', 'toaster', 'ngDialog', 'rootUrl', 'fileReader', '$q', 'utilService', 'cfpLoadingBar'];
    function PreRegistrationController($scope, $http, toaster, ngDialog, rootUrl, fileReader, $q, utilService, cfpLoadingBar) {

        var S3BaseUrl = 'https://s3-us-west-1.amazonaws.com/pro-manager/images/';
        $scope.article = {};
        $scope.prereg = {};
        $scope.prereggrid = false;
        $scope.preregId = null;
        $scope.Createpreregbutton = true;
        $scope.updatepreregbutton = false;
        $scope.hideoption3 = true;
        $scope.lstfile = [];
        $scope.lstpicname = [];
        $scope.hideoption4 = true;
        $scope.objpreregarray = [];
        $scope.prereg.answer = []
        $scope.lstIcons = [];
        $scope.icondisplay = "";

        $scope.getPreRegstrationList = function () {
            $http({
                method: 'get',
                url: rootUrl + '/api/admin/getQuestions',
                headers: { 'content-type': 'application/json' },
            })
                .success(function (data, status, headers, config) {
                    if (data.status == "success") {
                        $scope.prereglist = data.result;
                        $scope.prereggrid = true;
                    }
                    else {
                        $scope.prereggrid = false;
                        toaster.pop("error", "Pre-Registration", "Error while fetching preregistration list");
                    }
                })
                .error(function () {
                    toaster.pop("error", "Pre-Registration", "Error while fetching preregistration list");
                });
        }
        $scope.iconupload = function (index) {
            $scope.index = index;
        }

        $scope.uploadFile = function (event) {
            var files = event.target.files;
            $scope.pictureFile = files[0];

            if ($scope.pictureFile !== undefined) {

                $scope.pictureName = Date.now() + '.' + $scope.pictureFile.name.split('.').pop();
                fileReader.readAsDataUrl($scope.pictureFile, $scope)
                    .then(function (result) {
                        $scope.lstfile[$scope.index] = $scope.pictureFile;
                        $scope.lstpicname[$scope.index] = $scope.pictureName
                        if (event.target.id == "Icon1") {
                            $scope.article.picture1 = result;
                        }
                        else if (event.target.id == "Icon2") {
                            $scope.article.picture2 = result;
                        }
                        else if (event.target.id == "Icon3") {
                            $scope.article.picture3 = result;
                        }
                        else if (event.target.id == "Icon4") {
                            $scope.article.picture4 = result;
                        }
                    });
            }
        };

        $scope.submitbtn = function (prereglst, form) {
            if ($scope.article.picture1 && $scope.article.picture2) {
                var files = $scope.lstfile;
                var uploadedFile = [];
                // for (var i = 0; i < files.length; i++) {
                cfpLoadingBar.start();
                utilService.uploadFile(files, S3BaseUrl).then(
                    function (result) {

                        uploadedFile = angular.copy(result);

                        if (uploadedFile.length == files.length) {

                            $scope.lstIcons = uploadedFile;
                            $scope.objpreregarray = [];
                            var objOption = {};
                            for (var i = 0; i < prereglst.answer.length; i++) {
                                if (prereglst.answer[i].value) {
                                    objOption = {
                                        "value": prereglst.answer[i].value,
                                        "icon": $scope.lstIcons[i]
                                    }
                                    $scope.objpreregarray.push(objOption);
                                }
                            }
                            var objprereg = { question: prereglst.question, options: $scope.objpreregarray }
                            $http({
                                method: 'post',
                                url: rootUrl + '/api/admin/addQuestion',
                                headers: { 'content-type': 'application/json' },
                                data: objprereg
                            })
                                .success(function (data, status, headers, config) {
                                    if (data.status == "success") {
                                        $scope.prereg.question = "";
                                        $scope.prereg.answer = [];
                                        $scope.objpreregarray = [];

                                        $scope.editor = false;
                                        $scope.lstIcons = [];
                                        $scope.lstfile = [];
                                        $scope.hideoption3 = true;
                                        $scope.hideoption4 = true;
                                        $scope.getPreRegstrationList();
                                        form.$setPristine();
                                        form.$setUntouched();
                                        cfpLoadingBar.complete();
                                        toaster.pop("success", "Pre-Registration", "Preregistration detail added successfully");
                                    }
                                    else {
                                        toaster.pop("error", "Pre-Registration", "Error while adding preregistration");
                                    }
                                }).error(function () {
                                    toaster.pop("error", "Pre-Registration", "Error while adding preregistration");
                                });
                        }
                    }
                );
                // }
            }
            else {
                toaster.pop("error", "Pre-Registration", "Please select file for option1 and option2");
            }
        }

        $scope.updatebtn = function (prereglst, form) {
            cfpLoadingBar.start();
            var files = $scope.lstfile;
            var uploadedFile = [];
            var objOption = {};
            if (files.length > 0) {

                // for (var i = 0; i < files.length; i++) {
                utilService.uploadFile(files, S3BaseUrl).then(
                    function (result) {
                        uploadedFile = angular.copy(result);
                        if (uploadedFile.length == files.length) {

                            $scope.lstIcons = uploadedFile;

                            $scope.objpreregarray = [];

                            for (var i = 0; i < prereglst.answer.length; i++) {
                                if (prereglst.answer[i].value) {
                                    if ($scope.lstIcons[i]) {
                                        objOption = {
                                            "value": prereglst.answer[i].value,
                                            "icon": $scope.lstIcons[i]
                                        }
                                    }
                                    else {
                                        objOption = {
                                            "value": prereglst.answer[i].value,
                                            "icon": $scope.prereg.answer[i].icon
                                        }
                                    }
                                    $scope.objpreregarray.push(objOption);
                                }

                            }
                            var objprereg = { question: prereglst.question, options: $scope.objpreregarray }
                            $http({
                                method: 'put',
                                url: rootUrl + '/api/admin/editQuestion/' + prereglst.Id,
                                headers: { 'content-type': 'application/json' },
                                data: objprereg
                            })
                                .success(function (data, status, headers, config) {
                                    if (data.status == "success") {
                                        $scope.prereg.question = "";
                                        $scope.prereg.answer = [];
                                        $scope.Createpreregbutton = true;
                                        $scope.updatepreregbutton = false;
                                        $scope.editor = false;
                                        $scope.lstIcons = [];
                                        $scope.lstfile = [];
                                        $scope.hideoption3 = true;
                                        $scope.hideoption4 = true;
                                        $scope.getPreRegstrationList();
                                        form.$setPristine();
                                        form.$setUntouched();
                                        cfpLoadingBar.complete();
                                        toaster.pop("success", "Pre-Registration", "Preregistration detail updated successfully");
                                    }
                                    else {
                                        toaster.pop("error", "Pre-Registration", "Error while updating preregistration");
                                    }
                                }).error(function () {
                                    toaster.pop("error", "Pre-Registration", "Error while updating preregistration");
                                });

                        }
                    });
                // }
            }
            else {
                $scope.objpreregarray = [];
                for (var i = 0; i < prereglst.answer.length; i++) {
                    if (prereglst.answer[i].value) {
                        objOption = {
                            "value": prereglst.answer[i].value,
                            "icon": $scope.prereg.answer[i].icon
                        }
                        $scope.objpreregarray.push(objOption);
                    }

                }
                var objprereg = { question: prereglst.question, options: $scope.objpreregarray }
                $http({
                    method: 'put',
                    url: rootUrl + '/api/admin/editQuestion/' + prereglst.Id,
                    headers: { 'content-type': 'application/json' },
                    data: objprereg
                })
                    .success(function (data, status, headers, config) {
                        if (data.status == "success") {
                            $scope.prereg.question = "";
                            $scope.prereg.answer = [];
                            $scope.Createpreregbutton = true;
                            $scope.updatepreregbutton = false;
                            $scope.editor = false;
                            $scope.lstIcons = [];
                            $scope.lstfile = [];
                            $scope.hideoption3 = true;
                            $scope.hideoption4 = true;
                            $scope.getPreRegstrationList();
                            form.$setPristine();
                            form.$setUntouched();
                            cfpLoadingBar.complete();
                            toaster.pop("success", "Pre-Registration", "Preregistration detail updated successfully");
                        }
                        else {
                            toaster.pop("error", "Pre-Registration", "Error while updating preregistration");
                        }
                    }).error(function () {
                        toaster.pop("error", "Pre-Registration", "Error while updating preregistration");
                    });
            }

        }

        $scope.addoptionsbtn = function () {
            if (!$scope.hideoption3) {
                $scope.hideoption4 = false;
            }
            $scope.hideoption3 = false;
        }

        $scope.enablenewprereg = function (form) {
            form.$setPristine();
            form.$setUntouched();
            $scope.Createpreregbutton = true;
            $scope.updatepreregbutton = false;
            angular.element("input[type='file']").filestyle('clear');
            $scope.hideoption3 = true;
            $scope.hideoption4 = true;
            $scope.editor = true;
        }

        $scope.disablenewprereg = function (form) {
            form.$setPristine();
            form.$setUntouched();
            $scope.prereg.question = "";
            $scope.prereg.answer = [];
            $scope.editor = false;
            $scope.hideoption3 = true;
            $scope.hideoption4 = true;
        }

        $scope.disableupdateprereg = function (form) {
            $scope.Createpreregbutton = true;
            $scope.updatepreregbutton = false;
            form.$setPristine();
            form.$setUntouched();
            $scope.prereg = { answer: [] };
            $scope.editor = false;
            $scope.hideoption3 = true;
            $scope.hideoption4 = true;
        }

        $scope.Editbtn = function (prereglst) {

            $scope.prereg.question = prereglst.question;
            $scope.Createpreregbutton = false;
            $scope.updatepreregbutton = true;
            $scope.hideoption3 = prereglst.options.length >= 3 ? false : true;
            $scope.hideoption4 = prereglst.options.length == 4 ? false : true;
            $scope.prereg.answer = angular.copy(prereglst.options);
            $scope.prereg.Id = prereglst._id;
            $scope.editor = true;
            angular.element("input[type='file']").filestyle('clear');
        }

        $scope.Deletebtn = function (prereglst) {
            ngDialog.open({ template: 'firstDialogId', scope: $scope });
            $scope.preregId = prereglst._id;
        }

        $scope.showicon = function (option) {
            ngDialog.open({ template: 'IconPopup', scope: $scope });
            $scope.icondisplay = option.icon;
        }

        $scope.ConfirmDeletebtn = function (form) {
            $http({
                method: 'DELETE',
                url: rootUrl + '/api/admin/deleteQuestion/' + $scope.preregId,
                headers: { 'content-type': 'application/json' }
            })
                .success(function (data, status, headers, config) {
                    if (data.status == "success") {
                        form.$setPristine();
                        form.$setUntouched();
                        $scope.prereg.question = "";
                        $scope.prereg.answer = [];
                        $scope.updatepreregbutton = false;
                        $scope.Createpreregbutton = true;
                        $scope.editor = false;
                        $scope.hideoption3 = true;
                        $scope.hideoption4 = true;
                        $scope.getPreRegstrationList();
                        ngDialog.close();
                        toaster.pop("success", "Pre-Registration", "Preregistration detail deleted successfully");
                    }
                    else {
                        toaster.pop("error", "Pre-Registration", "Error while deleting preregistration");
                    }
                }).error(function (err) {
                    toaster.pop("error", "Pre-Registration", "Error while deleting preregistration");
                });
        }

        $scope.getPreRegstrationList();
    }
})();