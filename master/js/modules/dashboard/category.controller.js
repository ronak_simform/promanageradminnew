(function() {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('CategoryController', CategoryController)
        
    CategoryController.$inject = ['$rootScope', '$scope', '$state','$http','toaster','ngDialog','rootUrl'];
    function CategoryController($rootScope, $scope, $state,$http,toaster,ngDialog,rootUrl) {
            
            $scope.category = {};
            $scope.categorylist = {};
            $scope.CateId = "";

            $scope.getCategoryList = function () {
                $http({ 
                     method: 'get',
                     url: rootUrl + '/api/admin/getCategories',
                     headers: { 'content-type': 'application/json' },
                })
                .success(function (data, status, headers, config) {
                     if (data.status == "success") {
                       $scope.categorylist = data.result;
                       $scope.categorygrid = true;
                     }
                     else
                     {
                           $scope.categorygrid = false;
                           toaster.pop("error", "Category", "Error while fetching category list");
                     }
                })
                .error(function () {
                     toaster.pop("error", "Category", "Error while fetching category list");
                });        
            }

            $scope.submitbtn = function (categorylst,form) {

                var objcate = { name: categorylst.name }
                $http({
                     method: 'post',
                     url: rootUrl + '/api/admin/mangeCategory',
                     headers: { 'content-type': 'application/json'},
                     data: objcate
                })
                .success(function (data, status, headers, config) {
                     if (data.status == "success") {
                       $scope.category = {};
                       $scope.editor = false;
                       $scope.getCategoryList();
                       form.$setPristine();
                       form.$setUntouched();
                       toaster.pop("success", "Category", "Category added successfully");
                      }
                      else {
                        toaster.pop("error", "Category", "Error while adding category");
                       }
                }).error(function () {
                       toaster.pop("error", "Category", "Error while adding category");
                });

            }


            $scope.enablenewcategory = function (form) {
                form.$setPristine();
                form.$setUntouched();
                $scope.editor = true;
            }

            $scope.disablenewcategory = function (form) {
                form.$setPristine();
                form.$setUntouched();
                $scope.category = {};
                $scope.editor = false;
            }

            $scope.Deletebtn = function (categorylst) {
                ngDialog.open({ template: 'firstDialogId',scope: $scope});
                $scope.CateId = categorylst._id;
            }

            $scope.ConfirmDeletebtn = function (form) {
                var objcate = { categoryId :  $scope.CateId }
                $http({
                    method: 'DELETE',
                    url: rootUrl + '/api/admin/removeCategory',
                    headers: { 'content-type': 'application/json' },
                    data: objcate
                })
                .success(function (data, status, headers, config) {
                     if (data.status == "success") {
                        $scope.category = {};
                        $scope.editor = false;
                        $scope.getCategoryList();
                        ngDialog.close();
                        form.$setPristine();
                        form.$setUntouched();
                        toaster.pop("success", "Category", "Category deleted successfully");
                      }
                      else {
                        toaster.pop("error", "Category", "Error while deleting category");
                       }
                }).error(function (err) {
                       toaster.pop("error", "Category", "Error while deleting category");
                });
            }

            $scope.getCategoryList();
    }
})();