(function() {
    'use strict';

    angular
        .module('angle')
        .directive('ajaxCloak', ajaxCloak);

    ajaxCloak.$inject = ['$interval', '$http'];
    function ajaxCloak ($interval, $http) {
        return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    var stop = $interval(function() {
                        if ($http.pendingRequests.length === 0) {
                            $interval.cancel(stop);
                            attrs.$set("ajaxCloak", undefined);
                            element.removeClass("ajax-cloak");
                        }
                    }, 100);

                }
            };

    }

})();
