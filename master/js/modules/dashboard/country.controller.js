(function() {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('CountryController', CountryController);
        
    CountryController.$inject = ['$rootScope', '$scope', '$state','$http','toaster','ngTableParams','rootUrl'];
    function CountryController($rootScope, $scope, $state,$http,toaster,ngTableParams,rootUrl) {
         
            $scope.countrygrid = false;
            $scope.useParamPageNo = true;
            $scope.CountryList = {};

            $scope.countryBind = [
            { Id :  "AF", Name : "Afghanistan" },
            { Id :  "AL", Name : "Albania" },
            { Id :  "DZ", Name : "Algeria" },
            { Id :  "AS", Name : "American Samoa" },
            { Id :  "AD", Name : "Andorra" },
            { Id :  "AO", Name : "Angola" },
            { Id :  "AI", Name : "Anguilla" },
            { Id :  "AQ", Name : "Antarctica" },
            { Id :  "AG", Name : "Antigua and Barbuda" },
            { Id :  "AR", Name : "Argentina" },
            { Id :  "AM", Name : "Armenia" },
            { Id :  "AW", Name : "Aruba" },
            { Id :  "AU", Name : "Australia" },
            { Id :  "AT", Name : "Austria" },
            { Id :  "AZ", Name : "Azerbaijan" },
            { Id :  "BS", Name : "Bahamas" },
            { Id :  "BH", Name : "Bahrain" },
            { Id :  "BD", Name : "Bangladesh" },
            { Id :  "BB", Name : "Barbados" },
            { Id :  "BY", Name : "Belarus" },
            { Id :  "BE", Name : "Belgium" },
            { Id :  "BZ", Name : "Belize" },
            { Id :  "BJ", Name : "Benin" },
            { Id :  "BM", Name : "Bermuda" },
            { Id :  "BT", Name : "Bhutan" },
            { Id :  "BO", Name : "Bolivia" },
            { Id :  "BA", Name : "Bosnia and Herzegovina" },
            { Id :  "BW", Name : "Botswana" },
            { Id :  "BV", Name : "Bouvet Island" },
            { Id :  "BR", Name : "Brazil" },
            { Id :  "BQ", Name : "British Antarctic Territory" },
            { Id :  "IO", Name : "British Indian Ocean Territory" },
            { Id :  "VG", Name : "British Virgin Islands" },
            { Id :  "BN", Name : "Brunei" },
            { Id :  "BG", Name : "Bulgaria" },
            { Id :  "BF", Name : "Burkina Faso" },
            { Id :  "BI", Name : "Burundi" },
            { Id :  "KH", Name : "Cambodia" },
            { Id :  "CM", Name : "Cameroon" },
            { Id :  "CA", Name : "Canada" },
            { Id :  "CT", Name : "Canton and Enderbury Islands" },
            { Id :  "CV", Name : "Cape Verde" },
            { Id :  "KY", Name : "Cayman Islands" },
            { Id :  "CF", Name : "Central African Republic" },
            { Id :  "TD", Name : "Chad" },
            { Id :  "CL", Name : "Chile" },
            { Id :  "CN", Name : "China" },
            { Id :  "CX", Name : "Christmas Island" },
            { Id :  "CC", Name : "Cocos [Keeling] Islands" },
            { Id :  "CO", Name : "Colombia" },
            { Id :  "KM", Name : "Comoros" },
            { Id :  "CG", Name : "Congo - Brazzaville" },
            { Id :  "CD", Name : "Congo - Kinshasa" },
            { Id :  "CK", Name : "Cook Islands" },
            { Id :  "CR", Name : "Costa Rica" },
            { Id :  "HR", Name : "Croatia" },
            { Id :  "CU", Name : "Cuba" },
            { Id :  "CY", Name : "Cyprus" },
            { Id :  "CZ", Name : "Czech Republic" },
            { Id :  "CI", Name : "Côte d’Ivoire" },
            { Id :  "DK", Name : "Denmark" },
            { Id :  "DJ", Name : "Djibouti" },
            { Id :  "DM", Name : "Dominica" },
            { Id :  "DO", Name : "Dominican Republic" },
            { Id :  "NQ", Name : "Dronning Maud Land" },
            { Id :  "DD", Name : "East Germany" },
            { Id :  "EC", Name : "Ecuador" },
            { Id :  "EG", Name : "Egypt" },
            { Id :  "SV", Name : "El Salvador" },
            { Id :  "GQ", Name : "Equatorial Guinea" },
            { Id :  "ER", Name : "Eritrea" },
            { Id :  "EE", Name : "Estonia" },
            { Id :  "ET", Name : "Ethiopia" },
            { Id :  "FK", Name : "Falkland Islands" },
            { Id :  "FO", Name : "Faroe Islands" },
            { Id :  "FJ", Name : "Fiji" },
            { Id :  "FI", Name : "Finland" },
            { Id :  "FR", Name : "France" },
            { Id :  "GF", Name : "French Guiana" },
            { Id :  "PF", Name : "French Polynesia" },
            { Id :  "TF", Name : "French Southern Territories" },
            { Id :  "FQ", Name : "French Southern and Antarctic Territories" },
            { Id :  "GA", Name : "Gabon" },
            { Id :  "GM", Name : "Gambia" },
            { Id :  "GE", Name : "Georgia" },
            { Id :  "DE", Name : "Germany" },
            { Id :  "GH", Name : "Ghana" },
            { Id :  "GI", Name : "Gibraltar" },
            { Id :  "GR", Name : "Greece" },
            { Id :  "GL", Name : "Greenland" },
            { Id :  "GD", Name : "Grenada" },
            { Id :  "GP", Name : "Guadeloupe" },
            { Id :  "GU", Name : "Guam" },
            { Id :  "GT", Name : "Guatemala" },
            { Id :  "GG", Name : "Guernsey" },
            { Id :  "GN", Name : "Guinea" },
            { Id :  "GW", Name : "Guinea-Bissau" },
            { Id :  "GY", Name : "Guyana" },
            { Id :  "HT", Name : "Haiti" },
            { Id :  "HM", Name : "Heard Island and McDonald Islands" },
            { Id :  "HN", Name : "Honduras" },
            { Id :  "HK", Name : "Hong Kong SAR China" },
            { Id :  "HU", Name : "Hungary" },
            { Id :  "IS", Name : "Iceland" },
            { Id :  "IN", Name : "India" },
            { Id :  "ID", Name : "Indonesia" },
            { Id :  "IR", Name : "Iran" },
            { Id :  "IQ", Name : "Iraq" },
            { Id :  "IE", Name : "Ireland" },
            { Id :  "IM", Name : "Isle of Man" },
            { Id :  "IL", Name : "Israel" },
            { Id :  "IT", Name : "Italy" },
            { Id :  "JM", Name : "Jamaica" },
            { Id :  "JP", Name : "Japan" },
            { Id :  "JE", Name : "Jersey" },
            { Id :  "JT", Name : "Johnston Island" },
            { Id :  "JO", Name : "Jordan" },
            { Id :  "KZ", Name : "Kazakhstan" },
            { Id :  "KE", Name : "Kenya" },
            { Id :  "KI", Name : "Kiribati" },
            { Id :  "KW", Name : "Kuwait" },
            { Id :  "KG", Name : "Kyrgyzstan" },
            { Id :  "LA", Name : "Laos" },
            { Id :  "LV", Name : "Latvia" },
            { Id :  "LB", Name : "Lebanon" },
            { Id :  "LS", Name : "Lesotho" },
            { Id :  "LR", Name : "Liberia" },
            { Id :  "LY", Name : "Libya" },
            { Id :  "LI", Name : "Liechtenstein" },
            { Id :  "LT", Name : "Lithuania" },
            { Id :  "LU", Name : "Luxembourg" },
            { Id :  "MO", Name : "Macau SAR China" },
            { Id :  "MK", Name : "Macedonia" },
            { Id :  "MG", Name : "Madagascar" },
            { Id :  "MW", Name : "Malawi" },
            { Id :  "MY", Name : "Malaysia" },
            { Id :  "MV", Name : "Maldives" },
            { Id :  "ML", Name : "Mali" },
            { Id :  "MT", Name : "Malta" },
            { Id :  "MH", Name : "Marshall Islands" },
            { Id :  "MQ", Name : "Martinique" },
            { Id :  "MR", Name : "Mauritania" },
            { Id :  "MU", Name : "Mauritius" },
            { Id :  "YT", Name : "Mayotte" },
            { Id :  "FX", Name : "Metropolitan France" },
            { Id :  "MX", Name : "Mexico" },
            { Id :  "FM", Name : "Micronesia" },
            { Id :  "MI", Name : "Midway Islands" },
            { Id :  "MD", Name : "Moldova" },
            { Id :  "MC", Name : "Monaco" },
            { Id :  "MN", Name : "Mongolia" },
            { Id :  "ME", Name : "Montenegro" },
            { Id :  "MS", Name : "Montserrat" },
            { Id :  "MA", Name : "Morocco" },
            { Id :  "MZ", Name : "Mozambique" },
            { Id :  "MM", Name : "Myanmar [Burma]" },
            { Id :  "NA", Name : "Namibia" },
            { Id :  "NR", Name : "Nauru" },
            { Id :  "NP", Name : "Nepal" },
            { Id :  "NL", Name : "Netherlands" },
            { Id :  "AN", Name : "Netherlands Antilles" },
            { Id :  "NT", Name : "Neutral Zone" },
            { Id :  "NC", Name : "New Caledonia" },
            { Id :  "NZ", Name : "New Zealand" },
            { Id :  "NI", Name : "Nicaragua" },
            { Id :  "NE", Name : "Niger" },
            { Id :  "NG", Name : "Nigeria" },
            { Id :  "NU", Name : "Niue" },
            { Id :  "NF", Name : "Norfolk Island" },
            { Id :  "KP", Name : "North Korea" },
            { Id :  "VD", Name : "North Vietnam" },
            { Id :  "MP", Name : "Northern Mariana Islands" },
            { Id :  "NO", Name : "Norway" },
            { Id :  "OM", Name : "Oman" },
            { Id :  "PC", Name : "Pacific Islands Trust Territory" },
            { Id :  "PK", Name : "Pakistan" },
            { Id :  "PW", Name : "Palau" },
            { Id :  "PS", Name : "Palestinian Territories" },
            { Id :  "PA", Name : "Panama" },
            { Id :  "PZ", Name : "Panama Canal Zone" },
            { Id :  "PG", Name : "Papua New Guinea" },
            { Id :  "PY", Name : "Paraguay" },
            { Id :  "YD", Name : "People''s Democratic Republic of Yemen" },
            { Id :  "PE", Name : "Peru" },
            { Id :  "PH", Name : "Philippines" },
            { Id :  "PN", Name : "Pitcairn Islands" },
            { Id :  "PL", Name : "Poland" },
            { Id :  "PT", Name : "Portugal" },
            { Id :  "PR", Name : "Puerto Rico" },
            { Id :  "QA", Name : "Qatar" },
            { Id :  "RO", Name : "Romania" },
            { Id :  "RU", Name : "Russia" },
            { Id :  "RW", Name : "Rwanda" },
            { Id :  "RE", Name : "Réunion" },
            { Id :  "BL", Name : "Saint Barthélemy" },
            { Id :  "SH", Name : "Saint Helena" },
            { Id :  "KN", Name : "Saint Kitts and Nevis" },
            { Id :  "LC", Name : "Saint Lucia" },
            { Id :  "MF", Name : "Saint Martin" },
            { Id :  "PM", Name : "Saint Pierre and Miquelon" },
            { Id :  "VC", Name : "Saint Vincent and the Grenadines" },
            { Id :  "WS", Name : "Samoa" },
            { Id :  "SM", Name : "San Marino" },
            { Id :  "SA", Name : "Saudi Arabia" },
            { Id :  "SN", Name : "Senegal" },
            { Id :  "RS", Name : "Serbia" },
            { Id :  "CS", Name : "Serbia and Montenegro" },
            { Id :  "SC", Name : "Seychelles" },
            { Id :  "SL", Name : "Sierra Leone" },
            { Id :  "SG", Name : "Singapore" },
            { Id :  "SK", Name : "Slovakia" },
            { Id :  "SI", Name : "Slovenia" },
            { Id :  "SB", Name : "Solomon Islands" },
            { Id :  "SO", Name : "Somalia" },
            { Id :  "ZA", Name : "South Africa" },
            { Id :  "GS", Name : "South Georgia and the South Sandwich Islands" },
            { Id :  "KR", Name : "South Korea" },
            { Id :  "ES", Name : "Spain" },
            { Id :  "LK", Name : "Sri Lanka" },
            { Id :  "SD", Name : "Sudan" },
            { Id :  "SR", Name : "Suriname" },
            { Id :  "SJ", Name : "Svalbard and Jan Mayen" },
            { Id :  "SZ", Name : "Swaziland" },
            { Id :  "SE", Name : "Sweden" },
            { Id :  "CH", Name : "Switzerland" },
            { Id :  "SY", Name : "Syria" },
            { Id :  "ST", Name : "São Tomé and Príncipe" },
            { Id :  "TW", Name : "Taiwan" },
            { Id :  "TJ", Name : "Tajikistan" },
            { Id :  "TZ", Name : "Tanzania" },
            { Id :  "TH", Name : "Thailand" },
            { Id :  "TL", Name : "Timor-Leste" },
            { Id :  "TG", Name : "Togo" },
            { Id :  "TK", Name : "Tokelau" },
            { Id :  "TO", Name : "Tonga" },
            { Id :  "TT", Name : "Trinidad and Tobago" },
            { Id :  "TN", Name : "Tunisia" },
            { Id :  "TR", Name : "Turkey" },
            { Id :  "TM", Name : "Turkmenistan" },
            { Id :  "TC", Name : "Turks and Caicos Islands" },
            { Id :  "TV", Name : "Tuvalu" },
            { Id :  "UM", Name : "U.S. Minor Outlying Islands" },
            { Id :  "PU", Name : "U.S. Miscellaneous Pacific Islands" },
            { Id :  "VI", Name : "U.S. Virgin Islands" },
            { Id :  "UG", Name : "Uganda" },
            { Id :  "UA", Name : "Ukraine" },
            { Id :  "SU", Name : "Union of Soviet Socialist Republics" },
            { Id :  "AE", Name : "United Arab Emirates" },
            { Id :  "GB", Name : "United Kingdom" },
            { Id :  "US", Name : "United States" },
            { Id :  "ZZ", Name : "Unknown or Invalid Region" },
            { Id :  "UY", Name : "Uruguay" },
            { Id :  "UZ", Name : "Uzbekistan" },
            { Id :  "VU", Name : "Vanuatu" },
            { Id :  "VA", Name : "Vatican City" },
            { Id :  "VE", Name : "Venezuela" },
            { Id :  "VN", Name : "Vietnam" },
            { Id :  "WK", Name : "Wake Island" },
            { Id :  "WF", Name : "Wallis and Futuna" },
            { Id :  "EH", Name : "Western Sahara" },
            { Id :  "YE", Name : "Yemen" },
            { Id :  "ZM", Name : "Zambia" },
            { Id :  "ZW", Name : "Zimbabwe" },
            { Id :  "AX", Name : "Åland Islands" }
            ]

            $scope.getCountry = function () {
                    $scope.countryTable = new ngTableParams({page: 1,
                        count: 10}, {
                        counts: 0,
                        getData: function ($defer, params) {
                            var pageNo;

                            if ($scope.useParamPageNo) {
                                pageNo = params.page();
                                $scope.currentPageNo = params.page();
                            }
                            else {
                                $scope.useParamPageNo = true;
                                pageNo = $scope.currentPageNo;
                                params.page(pageNo);
                            }

                            $http({ 
                                method: 'get',
                                url: rootUrl + '/api/admin/getCountries?page=' + (pageNo - 1 ),
                                headers: { 'content-type': 'application/json' },
                            })
                            .success(function (data, status, headers, config) {
                                if (data.status == "success") {
                                    $scope.countrygrid = true;
                                    $scope.CountryList = data.result;
                                    $defer.resolve($scope.CountryList);
                                    params.total(parseInt(data.result_count));
                                }
                                else
                                {
                                    $scope.countrygrid = false;
                                    toaster.pop("error", "Country", "Error while fetching countrylist");
                                }
                             })
                             .error(function () {
                                    toaster.pop("error", "Country", "Error while fetching countrylist");
                             });
            }
        });
            }

            $scope.submitbtn = function (Countrylst) {
                var objcountry = { name: Countrylst.selectcountry.Name, status: true }
                $http({
                    method: 'put',
                    url: rootUrl + '/api/admin/manageCountry',
                    headers: { 'content-type': 'application/json' },
                    data: objcountry
                })
                .success(function (data, status, headers, config) {
                     if (data.status == "success") {
                       $scope.getCountry();
                       toaster.pop("success", "Country", "Country saved successfully");
                     }
                     else {
                       toaster.pop("error", "Country", "Country name already exists");
                     }
                }).error(function () {
                     toaster.pop("error", "Country", "Error while saving country");
                });
            }

            $scope.enablebtn = function (countrylst,status) {
                
                var objcountry = { name: countrylst.name, status: status, countryId: countrylst._id }
                $http({
                    method: 'put',
                    url: rootUrl + '/api/admin/manageCountry',
                    headers: { 'content-type': 'application/json'},
                    data: objcountry
                })
                .success(function (data, status, headers, config) {
                     if (data.status == "success") {
                        $scope.getCountry();
                        toaster.pop("success", "Country", "Country status change successfully");
                      }
                      else {
                        toaster.pop("success", "Country", "Error while changing status of country");
                       }
                }).error(function () {
                      toaster.pop("success", "Country", "Error while changing status of country");
                });
                
            }
            $scope.getCountry();
    }
})();