(function() {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('SocialSiteController', SocialSiteController);
        
    SocialSiteController.$inject = ['$rootScope', '$scope', '$state','$http','toaster','rootUrl'];
    function SocialSiteController($rootScope, $scope, $state,$http,toaster,rootUrl) {
            
            $scope.socialgrid = false;
            $scope.SocialSiteList = {};
            
            $scope.getSocial = function () {
                $http({
                    method: 'get',
                    url: rootUrl + '/api/auth/getSocial',
                    headers: { 'content-type': 'application/json' },
                })
                .success(function (data, status, headers, config) {
                     if (data.status == "success") {
                        $scope.socialgrid = true;
                        $scope.SocialSiteList = data.result;
                      }
                      else {
                        $scope.socialgrid = false;
                        toaster.pop("error", "Social Connection", "Error while fetching socialsite");
                      }
                })
                .error(function () {
                      toaster.pop("error", "Social Connection", "Error while fetching socialsite");
                });
            }

            $scope.enablebtn = function (sociallst, status) {

                var objsite = { name: sociallst.name, status: status, socialId: sociallst._id }
                
                $http({
                    method: 'put',
                    url: rootUrl + '/api/admin/manageSocial',
                    headers: { 'content-type': 'application/json'},
                    data: objsite
                })
                .success(function (data, status, headers, config) {
                     if (data.status == "success") {
                        $scope.getSocial();
                        toaster.pop("success", "Social Connection", "SocialSite status change successfully");
                     }
                     else {
                         toaster.pop("error", "Social Connection", "Error while changing status of socialsite");
                     }
                }).error(function () {
                     toaster.pop("error", "Social Connection", "Error while changing status of socialsite");
                });

            }

            $scope.getSocial();
    }
})();