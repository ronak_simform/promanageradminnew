(function() {
    'use strict';

    angular
        .module('angle')
        .directive('customOnChange', customOnChange);

   function customOnChange() {
      
    var directive = {
        restrict: 'A',
        link: link
    };

    return directive;

    function link(scope, element, attrs) {
        var onChangeHandler = scope.$eval(attrs.customOnChange);
        element.bind('change', onChangeHandler);
    }
  } 
   

})();

