(function() {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('UserController', UserController);
        
    UserController.$inject = ['$rootScope', '$scope', '$state','$http','authService','toaster','ngTableParams','rootUrl'];
    function UserController($rootScope, $scope, $state,$http,authService,toaster,ngTableParams,rootUrl) {

            $scope.userlist = {};
            $scope.totalChannel = null;
            $scope.totalwaitinglist = null;
            $scope.totalcannel = null;
            $scope.userlistgrid = false;
            $scope.useParamPageNo = true;

       $scope.getdashboard = function () {
                $http({
                    method: 'get',
                    url: rootUrl + '/api/admin/getStats',
                    headers: { 'content-type': 'application/json' },
                })
                .success(function (data, status, headers, config) {
                     if (data.status == "success") {
                        $scope.totalChannel = data.result.totalChannels;
                        $scope.totalwaitinglist = data.result.totalWaitingList;
                        $scope.totalcannel = 0;
                     }
                     else {
                        toaster.pop("error", "User Dashboard", "Error while fetching channel and waitinglist");
                        }
                })
                .error(function () {
                     toaster.pop("error", "User Dashboard", "Error while fetching channel and waitinglist");
                });
            }

            $scope.getUserList = function () {

                    $scope.usersTable = new ngTableParams({page: 1,
                        count: 10}, {
                        counts: 0,
                        getData: function ($defer, params) {
                            var pageNo;

                            if ($scope.useParamPageNo) {
                                pageNo = params.page();
                                $scope.currentPageNo = params.page();
                            }
                            else {
                                $scope.useParamPageNo = true;
                                pageNo = $scope.currentPageNo;
                                params.page(pageNo);
                            }

                            $http({ 
                                method: 'get',
                                url: rootUrl + '/api/admin/getusers?page=' + (pageNo -1 ),
                                headers: { 'content-type': 'application/json' },
                            })
                            .success(function (data, status, headers, config) {
                                if (data.status == "success") {
                                   $scope.userlist = data.result;
                                   $scope.userlistgrid = true;
                                   $defer.resolve($scope.userlist);
                                  params.total(parseInt(data.result_count));
                                }
                                else
                                {
                                    toaster.pop("error", "User Dashboard", "Error while fetching userlist");
                                    $scope.userlistgrid = false;
                                }
                            })
                            .error(function () {
                                 toaster.pop("error", "User Dashboard", "Error while fetching userlist");  
                            });
            }
        });
            }
            $scope.getdashboard();
            $scope.getUserList();
    }
})();