﻿(function() {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('WaitingListController', WaitingListController);
        
    WaitingListController.$inject = ['$rootScope', '$scope', '$state','$http','toaster','ngTableParams','rootUrl'];
    function WaitingListController($rootScope, $scope, $state,$http,toaster,ngTableParams,rootUrl) {
            
            $scope.waitinglistgrid = false;
            $scope.Waitinglist = {};
            $scope.useParamPageNo = true;

            $scope.getWaitingList = function () {

                $scope.waitinglistTable = new ngTableParams({page: 1,
                        count: 10}, {
                        counts: 0,
                        getData: function ($defer, params) {
                            var pageNo;

                            if ($scope.useParamPageNo) {
                                pageNo = params.page();
                                $scope.currentPageNo = params.page();
                            }
                            else {
                                $scope.useParamPageNo = true;
                                pageNo = $scope.currentPageNo;
                                params.page(pageNo);
                            }

                            $http({ 
                                method: 'get',
                                url: rootUrl + '/api/admin/getWaitingList?page=' + (pageNo - 1 ),
                                headers: { 'content-type': 'application/json' },
                            })
                            .success(function (data, status, headers, config) {
                                if (data.status == "success") {
                                    $scope.waitinglistgrid = true;
                                    $scope.Waitinglist = data.result;
                                    $defer.resolve($scope.Waitinglist);
                                    params.total(parseInt(data.result_count));
                                }
                                else
                                {
                                    $scope.waitinglistgrid = false;
                                    toaster.pop("error", "Waiting List", "Error while fetching waitinglist");
                                }
                            })
                            .error(function () {
                                toaster.pop("error", "Waiting List", "Error while fetching waitinglist");
                             });
            }
        });
            }

            $scope.getWaitingList();
    }
})();