(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .factory('utilService', utilService);

    utilService.$inject = ['$log', '$http', 'rootUrl', '$q'];

    function utilService($log, $http, rootUrl, $q) {
        var service = {
            GetPreSignedURL: GetPreSignedURL,
            uploadFile: uploadFile
        };
        return service;

        //Generate Pre-Signed URL for file
        function GetPreSignedURL(fileName, file) {

            var reqData = {
                filename: fileName,
                contentType: file.type
            }
            return $http({
                method: 'POST',
                url: rootUrl + '/api/auth/preSignUrl',
                data: reqData
            });
        }


        function uploadFile(file, S3BaseUrl) {
            var postData = [];
            var currentRequest = 0;
            var deferred = $q.defer();
            var results = []

            makeNextRequest();

            function makeNextRequest() {
                postData = file[currentRequest];
                
                if (postData) {
                    var fileName = Date.now() + postData.name;
                    GetPreSignedURL(fileName, postData).then(

                        function (response) {

                            var imageUrl = response.data.url;

                            $http.put(imageUrl, postData, {
                                headers: { 'Content-Type': postData.type }
                            }).then(function (resp) {


                                var temp = S3BaseUrl + fileName;
                                results.push(temp);

                                currentRequest++;
                                if (currentRequest < file.length) {
                                    makeNextRequest();
                                }
                                else {
                                    deferred.resolve(results);
                                }
                            });
                        },
                        function (error) {
                            $log.info('error in file upload function', error);
                        }
                    )
                }
                else {
                    currentRequest++;
                    results.push(undefined);
                    if (currentRequest < file.length) {
                        makeNextRequest();
                    }
                }
            }
            return deferred.promise;
        }

    }
})();