(function() {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('PlaceholderController', PlaceholderController);
        
    PlaceholderController.$inject = ['$scope','$http','toaster','fileReader','$sce','rootUrl'];
    function PlaceholderController($scope,$http,toaster,fileReader,$sce,rootUrl) {
            
            var S3BaseUrl='https://s3-us-west-1.amazonaws.com/pro-manager/images/';
            $scope.article = {};
            $scope.placeholder = {};


            $scope.getplaceholderimage = function () {
                $http({
                    method: 'get',
                    url: rootUrl + '/api/auth/getPlaceHolder',
                    headers: { 'content-type': 'application/json'},
                })
                .success(function (data, status, headers, config) {
                    if (data.status == "success") {
                        $scope.placeholder = data.result;
                        $scope.placeholder.video = $sce.trustAsResourceUrl($scope.placeholder.video);
                    }
                    else {
                        $scope.tableage = false;
                        toaster.pop("error", "Placeholder", "Error while fetching profile picture,cover picture,video");
                    }
                })
                .error(function () {
                    toaster.pop("error", "Placeholder", "Error while fetching profile picture,cover picture,video");
                });
            }

            $scope.uploadFile = function (event) {
	            var files = event.target.files;
	            $scope.pictureFile= files[0];
	            $scope.pictureName = Date.now() + '.' + $scope.pictureFile.name.split('.').pop();
	            fileReader.readAsDataUrl($scope.pictureFile, $scope)
        			  .then(function (result) {
		        		  $scope.article.picture = result;
			        });
            };

            $scope.upload = function (mode) {
              if($scope.pictureFile)
               {
                    $http.post(rootUrl + '/api/auth/preSignUrl', {
                        filename: $scope.pictureName,
                        contentType: $scope.pictureFile.type
                    }).success(function (resp) {
                    $http.put(resp.url, $scope.pictureFile, {headers: {'Content-Type': $scope.pictureFile.type}})
                        .success(function (resp) {
                            var objprofileimg = {};
                            $scope.article.picture = S3BaseUrl + $scope.pictureName;
                            
                            if(mode == "profilepic")
                            {
                                objprofileimg = { profilePic :  $scope.article.picture }
                            }
                            else if(mode == "coverpic")
                            {
                                objprofileimg = { coverPic :  $scope.article.picture }
                            }
                            else if(mode == "video")
                            {
                                objprofileimg = { video :  $scope.article.picture }
                            }

                            $http({
                                method: 'post',
                                url: rootUrl + '/api/admin/mangePlaceHolder',
                                headers: { 'content-type': 'application/json' },
                                data: objprofileimg
                                }).success(function (resp) {
                                    $scope.getplaceholderimage();
                                    if(mode == "profilepic")
                                    {
                                        toaster.pop("success", "Placeholder", "Profile picture uploaded successfully");
                                    }
                                    else if(mode == "coverpic")
                                    {
                                        toaster.pop("success", "Placeholder", "Cover picture uploaded successfully");
                                    }       
                                    else if(mode == "video")
                                    {
                                        toaster.pop("success", "Placeholder", "video uploaded successfully");
                                    }   
                                
                                    })
                                .error(function (resp) {
                                    toaster.pop("error", "Placeholder", "An Error occurred while saving file");
                                });
                        })
                        .error(function (resp) {
                            toaster.pop("error", "Placeholder", "An Error occurred while saving file");
                        });
                    }).error(function (resp) {
                        toaster.pop("error", "Placeholder", "An Error occurred while saving file");
                    });
                }
                else
                {
                        toaster.pop("error", "Placeholder", "Please select file");
                }       
            }

            $scope.tweettextbtn = function () {
                var objtweettext = { tweetText : $scope.placeholder.tweetText }
                $http({
                    method: 'post',
                    url: rootUrl + '/api/admin/mangePlaceHolder',
                    headers: { 'content-type': 'application/json' },
                    data: objtweettext
                })
                .success(function (data, status, headers, config) {
                    if (data.status == "success") {
                        $scope.placeholder.tweetText = data.result.tweetText;
                        toaster.pop("success", "Placeholder", "Tweet text saved successfully");
                    }
                    else {
                        toaster.pop("error", "Placeholder", "Error while saving tweet text detail");
                    }

                }).error(function () {
                    toaster.pop("error", "Placeholder", "Error while saving tweet text detail");
                });
            }

            $scope.posttextbtn = function () {
                var objposttext = { character_limit : $scope.placeholder.character_limit }
                $http({
                    method: 'post',
                    url: rootUrl + '/api/admin/mangePlaceHolder',
                    headers: { 'content-type': 'application/json' },
                    data: objposttext
                })
                .success(function (data, status, headers, config) {
                    if (data.status == "success") {
                        $scope.postText = data.result.character_limit;
                        toaster.pop("success", "Post", "Limit of text detail saved successfully");
                    }
                    else {
                        toaster.pop("error", "Post", "Error while saving limit of text detail");
                    }

                }).error(function () {
                    toaster.pop("error", "Post", "Error while saving limit of text detail");
                });
            }

            $scope.postphotobtn = function () {
                var objpostphoto = { photo_limit : $scope.placeholder.photo_limit }
                $http({
                    method: 'post',
                    url: rootUrl + '/api/admin/mangePlaceHolder',
                    headers: { 'content-type': 'application/json' },
                    data: objpostphoto
                })
                .success(function (data, status, headers, config) {
                    if (data.status == "success") {
                        $scope.postText = data.result.photo_limit;
                        toaster.pop("success", "Post", "Number of photos detail saved successfully");
                    }
                    else {
                        toaster.pop("error", "Post", "Error while saving number of photos detail");
                    }

                }).error(function () {
                    toaster.pop("error", "Post", "Error while saving number of photos detail");
                });
            }

       $scope.getplaceholderimage();
            
    }
   
})();