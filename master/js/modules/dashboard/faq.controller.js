(function() {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('FAQController', FAQController)
        
    FAQController.$inject = ['$rootScope', '$scope', '$state','$http','toaster','ngDialog','ngTableParams','rootUrl'];
    function FAQController($rootScope, $scope, $state,$http,toaster,ngDialog,ngTableParams,rootUrl) {
            
            $scope.faq = {};
            $scope.faqgrid = false;
            $scope.faqId = null;
            $scope.Createfaqbutton = true;
            $scope.updatefaqbutton = false;
            $scope.useParamPageNo = true;

            $scope.getFAQList = function () {
                    $scope.faqTable = new ngTableParams({page: 1,
                        count: 10}, {
                        counts: 0,
                        getData: function ($defer, params) {
                            var pageNo;

                            if ($scope.useParamPageNo) {
                                pageNo = params.page();
                                $scope.currentPageNo = params.page();
                            }
                            else {
                                $scope.useParamPageNo = true;
                                pageNo = $scope.currentPageNo;
                                params.page(pageNo);
                            }

                            $http({ 
                            method: 'get',
                            url: rootUrl + '/api/admin/faqs?page=' + (pageNo - 1 ),
                            headers: { 'content-type': 'application/json' },
                            })
                            .success(function (data, status, headers, config) {
                                if (data.status == "success") {
                                    $scope.faqlist = data.result;
                                    $scope.faqgrid = true;
                                    $defer.resolve($scope.faqlist);
                                    params.total(parseInt(data.result_count));
                                }
                                else
                                {
                                    $scope.faqgrid = false;
                                    toaster.pop("error", "FAQ", "Error while fetching faq list");
                                }
                             })
                             .error(function () {
                                    toaster.pop("error", "FAQ", "Error while fetching faq list");
                   });
            }
        });
            }

            $scope.submitbtn = function (faqlst,form) {

                var objfaq = { title: faqlst.question, description: faqlst.answer }
                $http({
            method: 'post',
            url: rootUrl + '/api/admin/faqs',
            headers: { 'content-type': 'application/json'},
            data: objfaq
        })
                   .success(function (data, status, headers, config) {
                       if (data.status == "success") {
                           $scope.faq.question = "";
                           $scope.faq.answer = "";
                           $scope.editor = false;
                           $scope.getFAQList();
                           form.$setPristine();
                           form.$setUntouched();
                           toaster.pop("success", "FAQ", "FAQ added successfully");
                       }
                       else {
                           toaster.pop("error", "FAQ", "Error while adding faq");
                       }
                   }).error(function () {
                       toaster.pop("error", "FAQ", "Error while adding faq");
                   });

            }


            $scope.updatebtn = function (faqlst,form) {

                var objfaq = { title: faqlst.question, description: faqlst.answer }
                $http({
                    method: 'put',
                    url: rootUrl + '/api/admin/faqs/' + faqlst.Id,
                    headers: { 'content-type': 'application/json' },
                    data: objfaq
                })
                .success(function (data, status, headers, config) {
                     if (data.status == "success") {
                       $scope.faq.question = "";
                       $scope.faq.answer = "";
                       $scope.Createfaqbutton = true;
                       $scope.updatefaqbutton = false;
                       $scope.editor = false;
                       $scope.getFAQList();
                       form.$setPristine();
                       form.$setUntouched();
                       toaster.pop("success", "FAQ", "FAQ updated successfully");
                     }
                     else {
                       toaster.pop("error", "FAQ", "Error while updating faq");
                       }
                }).error(function () {
                     toaster.pop("error", "FAQ", "Error while updating faq");
                });
            }

            $scope.enablenewfaq = function (form) {
                form.$setPristine();
                form.$setUntouched();
                $scope.Createfaqbutton = true;
                $scope.updatefaqbutton = false;
                $scope.editor = true;
            }

            $scope.disablenewfaq = function (form) {
                form.$setPristine();
                form.$setUntouched();
                $scope.faq = {};
                $scope.editor = false;
            }

             $scope.disableupdatefaq = function (form) {
                $scope.Createfaqbutton = true;
                $scope.updatefaqbutton = false;
                form.$setPristine();
                form.$setUntouched();
                $scope.faq = {};
                $scope.editor = false;
            }

            $scope.Editbtn = function (faqlst) {
                $scope.faq.question = faqlst.title;
                $scope.Createfaqbutton = false;
                $scope.updatefaqbutton = true;
                $scope.faq.answer = faqlst.description;
                $scope.faq.Id = faqlst._id;
                $scope.editor = true;
            }

            $scope.Deletebtn = function (faqlst) {
                ngDialog.open({ template: 'firstDialogId',scope: $scope});
                $scope.faqId = faqlst._id;
            }

            $scope.ConfirmDeletebtn = function (form) {
                $http({
                    method: 'DELETE',
                    url: rootUrl + '/api/admin/faqs/' + $scope.faqId,
                    headers: { 'content-type': 'application/json' }
                })
                .success(function (data, status, headers, config) {
                     if (data.status == "success") {
                        form.$setPristine();
                        form.$setUntouched();
                        $scope.faq.question = "";
                        $scope.faq.answer = "";
                        $scope.updatefaqbutton = false;
                        $scope.Createfaqbutton = true;
                        $scope.editor = false;
                        $scope.getFAQList();
                        ngDialog.close();
                        toaster.pop("success", "FAQ", "FAQ deleted successfully");
                     }
                     else {
                        toaster.pop("error", "FAQ", "Error while deleting faq");
                     }
                }).error(function (err) {
                     toaster.pop("error", "FAQ", "Error while deleting faq");
                });
            }

            $scope.getFAQList();
    }
})();