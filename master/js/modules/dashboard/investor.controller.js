(function() {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('InvestorController', InvestorController);
        
    InvestorController.$inject = ['$rootScope', '$scope', '$state','$http','toaster','ngTableParams','rootUrl'];
    function InvestorController($rootScope, $scope, $state,$http,toaster,ngTableParams,rootUrl) {
            $scope.investorlist = {};
            $scope.totalChannel = null;
            $scope.totalwaitinglist = null;
            $scope.totalinvestor = null;
            $scope.investor = {};
            $scope.investorgrid = false;
            $scope.useParamPageNo = true;

             $scope.GenderDropBind = [
                { Id :  "1", Name : "Male" },
                { Id :  "2", Name : "Female" }
             ]

            $scope.getdashboard = function () {
                $http({
                        method: 'get',
                        url: rootUrl + '/api/admin/getStats',
                        headers: { 'content-type': 'application/json' },
                })
                .success(function (data, status, headers, config) {
                     if (data.status == "success") {
                        $scope.totalChannel = data.result.totalChannels;
                        $scope.totalwaitinglist = data.result.totalWaitingList;
                        $scope.totalinvestor = data.result.totalInvestors;
                     }
                     else {
                        toaster.pop("error", "Investor Dashboard", "Error while fetching channel and waitinglist");
                     }
                })
                .error(function () {
                     toaster.pop("error", "Investor Dashboard", "Error while fetching channel and waitinglist");
                });
            }
            $scope.getInvestorList = function () {

                    $scope.investorTable = new ngTableParams({page: 1,
                        count: 10}, {
                        counts: 0,
                        getData: function ($defer, params) {
                            var pageNo;

                            if ($scope.useParamPageNo) {
                                pageNo = params.page();
                                $scope.currentPageNo = params.page();
                            }
                            else {
                                $scope.useParamPageNo = true;
                                pageNo = $scope.currentPageNo;
                                params.page(pageNo);
                            }

                            $http({ 
                                method: 'get',
                                url: rootUrl + '/api/admin/getInvestors?page=' + (pageNo - 1 ),
                                headers: { 'content-type': 'application/json' },
                            })
                            .success(function (data, status, headers, config) {
                                if (data.status == "success") {
                                     $scope.investorlist = data.result;
                                     $scope.investorgrid = true;
                                    $defer.resolve($scope.investorlist);
                                    params.total(parseInt(data.result_count));
                                }
                                else
                                {
                                     $scope.investorgrid = false;
                                     toaster.pop("error", "Investor Dashboard", "Error while fetching investorlist");
                                }
                             })
            }
        });
            }

            $scope.submitbtn = function (investorlst,form) {
                $scope.investor.firstName = investorlst.UserFirstName;
                $scope.investor.lastName = investorlst.UserLastName;
                $scope.investor.email = investorlst.UserEmail;
                $scope.investor.password = investorlst.UserPassword;
                $scope.investor.gender = investorlst.selectedGender.Name;
                
                $scope.investor.roles = "investor";
                var objinvestor = { email: $scope.investor.email, firstName: $scope.investor.firstName, lastName: $scope.investor.lastName, password: $scope.investor.password, channelId: $scope.investor.email, roles: $scope.investor.roles,gender : $scope.investor.gender }
                $http({
                    method: 'post',
                    url: rootUrl + '/api/admin/addInvestor',
                    headers: { 'content-type': 'application/json' },
                    data: objinvestor
                })
                .success(function (data, status, headers, config) {
                    if (data.status == "success") {
                        form.$setPristine();
                        form.$setUntouched();
                        $scope.investor = {};
                        $scope.editor =  false;
                        $scope.getInvestorList();
                        toaster.pop("success", "Investor Dashboard", "Investor detail saved successfully");
                    }
                    else {
                        toaster.pop("error", "Investor Dashboard", "Investor with this mail id already exists");
                    }

                }).error(function () {
                    toaster.pop("error", "Investor Dashboard", "Error while saving investor detail");
                });
            }

            $scope.enablenewinvestor = function (form) {
                form.$setPristine();
                form.$setUntouched();
                $scope.investor = {};
                $scope.editor = true;
            }

            $scope.disablenewinvestor = function (form) {
                form.$setPristine();
                form.$setUntouched();
                $scope.investor = {}; 
                $scope.editor = false;
            }

            $scope.getdashboard();
            $scope.getInvestorList();
    }
})();