(function() {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('FavouriteListController', FavouriteListController);
        
    FavouriteListController.$inject = ['$rootScope', '$scope', '$state','$http','toaster','ngTableParams','rootUrl'];
    function FavouriteListController($rootScope, $scope, $state,$http,toaster,ngTableParams,rootUrl) {
            
            $scope.favouritelistgrid = false;
            $scope.Favouritelist = {};
            $scope.useParamPageNo = true;

            function favouritesCollection()
            {
                $scope.favouritelistTable = new ngTableParams({page: 1,
                        count: 10}, {
                        counts: 0,
                        getData: function ($defer, params) {
                            var pageNo;

                            if ($scope.useParamPageNo) {
                                pageNo = params.page();
                                $scope.currentPageNo = params.page();
                            }
                            else {
                                $scope.useParamPageNo = true;
                                pageNo = $scope.currentPageNo;
                                params.page(pageNo);
                            }
                            var objfav = { channelName : $scope.channelname , page : (pageNo - 1 )  }
                            $http({ 
                                method: 'post',
                                url: rootUrl + '/api/admin/getDefaultChannels',
                                headers: { 'content-type': 'application/json' },
                                data: objfav
                            })
                            .success(function (data, status, headers, config) {
                                if (data.status == "success") {
                                    $scope.favouritelistgrid = true;
                                    $scope.Favouritelist = data.result;
                                    $defer.resolve($scope.Favouritelist);
                                    params.total(parseInt(data.result_count));
                                }
                                else
                                {
                                    $scope.favouritelistgrid = false;
                                    toaster.pop("error", "Favourite List", "Error while fetching favouritelist");
                                }
                            })
                            .error(function () {
                                    toaster.pop("error", "Favourite List", "Error while fetching favouritelist");
                            });
                        }
                });

            }

            $scope.getFavouriteList = function () {

                favouritesCollection();
            }

            $scope.enablebtn = function (status,userlst) {
                var objfavourite = { isDefault: status }
                $http({
                    method: 'put',
                    url: rootUrl + '/api/admin/setDefault',
                    headers: { 'content-type': 'application/json','userid' :  userlst._id },
                    data: objfavourite
                })
                .success(function (data, status, headers, config) {
                     if (data.status == "success") {
                        $scope.getFavouriteList();
                        toaster.pop("success", "Favourite List", "Favourite status change successfully");
                      }
                      else {
                        toaster.pop("error", "Favourite List", "Error while changing status of favourite");
                      }
                }).error(function () {
                      toaster.pop("error", "Favourite List", "Error while changing status of favourite");
                });

            }

            $scope.searchbtn = function () {

                favouritesCollection();
            }

            $scope.getFavouriteList();
    }
})();