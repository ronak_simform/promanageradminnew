(function() {
    'use strict';

    angular
        .module('app.pages')
        .controller('LoginFormController', LoginFormController);

    LoginFormController.$inject = ['$http', '$state','$scope','authService','toaster','$cookies'];
    function LoginFormController($http, $state,$scope,authService,toaster,$cookies) {
  
            $scope.errormsg = false;
            $scope.editor = true;
            $scope.Forgot = {};
            $scope.signIn = function () {
               
                if ($scope.loginData) {
                    var pwd = $scope.loginData.password;
                    if (pwd) {
                        authService.login($scope.loginData).then(function (response) {
                            if (response.status == "success")
                            {
                                if ($scope.loginData.rememberMe) {
                                    $cookies.putObject("spinachcafeUserName",  $scope.loginData.userName );
                                    $cookies.putObject("spinachcafePwd",  $scope.loginData.password );
                                } else {
                                    $cookies.remove("spinachcafeUserName",  $scope.loginData.userName );
                                    $cookies.remove("spinachcafePwd",  $scope.loginData.password );
                                }
                                $state.go('app.dashboarduser');
                            }
                            else{
                                    toaster.pop("error", "Login Failed", "Username or password is incorrect.");
                            }
                        },
                        function (){
                            toaster.pop("error", "Login Failed", "Username or password is incorrect.");
                        });
                    } else {
                        toaster.pop("error", "Login Failed", "Enter valid password");
                    }
                } else {
                  toaster.pop("error", "Login Failed", "Enter valid username");
                }

            }

    var authData = $cookies.getObject('authorizationData');
    var uName = $cookies.getObject('spinachcafeUserName');
    var pwd = $cookies.getObject('spinachcafePwd');


    if (!authData) {
        if (uName != null && uName != undefined && uName != "") {
            if (pwd != null && pwd != undefined && pwd != "") {
                $scope.loginData = {
                    userName: uName,
                    password: pwd,
                    rememberMe: true
                };
            }
        }
    } else {
        if (uName != null && uName != undefined && uName != "") {
            if (pwd != null && pwd != undefined && pwd != "") {
                $scope.loginData = {
                    userName: uName,
                    password: pwd,
                    rememberMe: true
                };
                $scope.signIn();
            }
        }
    }
    }
})();
