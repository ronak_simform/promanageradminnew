(function() {
    'use strict';

    angular
        .module('app.pages')
        .controller('ForgottController', ForgottController);

    ForgottController.$inject = ['$http', '$state','$scope','toaster','rootUrl'];
    function ForgottController($http, $state,$scope,toaster,rootUrl) {
            $scope.errormsg = false;
            $scope.editor = true;
            $scope.Forgot = {};

            $scope.backToLogin = function (loginform) {
                $state.go('page.login');
            }

            $scope.forgetPasswordSubmit = function () {
                var objuser = { email: $scope.Forgot.Useremail }
                $http({
                    method: 'post',
                    url: rootUrl + '/api/auth/forgot',
                    headers: { 'content-type': 'application/json' },
                    data: objuser
                })
                .success(function (data, status, headers, config) {
                    toaster.pop("success", "Forgot Password", "An email has been sent to the provided email with further instructions");
                    $scope.editor = true;
                   
                }).error(function () {
                    toaster.pop("error", "Forgot Password", "No account with that username has been found");
                });
            }
    }
})();
