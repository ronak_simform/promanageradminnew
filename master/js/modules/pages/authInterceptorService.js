(function() {
    'use strict';

    angular
        .module('angle')
        .factory('authInterceptorService', authInterceptorService);

    authInterceptorService.$inject = ['$q','$cookies','$injector'];
    function authInterceptorService($q,$cookies,$injector) {
       
        var authInterceptorServiceFactory = {};

    var _request = function (config) {

        config.headers = config.headers || {};

        var authData = $cookies.getObject('authorizationData');
        if (authData) {
            config.headers["x-access-token"] = authData.token;
        }
        return config;
    }

    var _responseError = function (rejection) {
        var $state = $injector.get("$state");
        if (rejection.status === 401) {
            $state.go("page.login");
        }
        return $q.reject(rejection);
    }

    authInterceptorServiceFactory.request = _request;
    authInterceptorServiceFactory.responseError = _responseError;

    return authInterceptorServiceFactory;
    }
})();