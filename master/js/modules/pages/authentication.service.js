(function() {
    'use strict';

    angular
        .module('angle')
        .factory('authService', authService);

    authService.$inject = ['$http','$q','$location','$cookies','$rootScope','rootUrl'];
    function authService($http,$q,$location,$cookies,$rootScope,rootUrl) {

        
    var authServiceFactory = {};

    var _authentication = {
        isAuth: false,
        userName: ""
    };

    var _login = function (loginData) {
        $rootScope.principal = {};
        var requestData = { username: loginData.userName, password: loginData.password , admin : true };
        var deferred = $q.defer();

        $http({
            method: 'POST',
            url: rootUrl + '/api/auth/adminLogin',
            headers: { 'Content-Type': 'application/json' },
            data: requestData
        }).success(function (response) {
            if (response.status == "success") {

                $rootScope.principal.token = response.token;
                $rootScope.principal.userName = loginData.userName;
                $rootScope.principal.role = response.result.roles[0];
                $rootScope.principal.firstName = response.result.firstName;
                $cookies.putObject("authorizationData",  $rootScope.principal );
                _authentication.isAuth = true;
                _authentication.userName = loginData.userName;
            }
            deferred.resolve(response);

        }).error(function (err, status) {
            _logOut();
            deferred.reject(err);
        });

        return deferred.promise;

    };

    var _logOut = function () {

        $cookies.remove('authorizationData');
        _authentication.isAuth = false;
        _authentication.userName = "";

    };

    var _fillAuthData = function () {

        var authData = $cookies.get('authorizationData');
        if (authData) {
            _authentication.isAuth = true;
            _authentication.userName = authData.userName;
        }

    }

    var _checkLogin = function () {
        var authData = $cookies.get('authorizationData');
        if (authData) {
            return true;
        }
        return false;
    }

    authServiceFactory.login = _login;
    authServiceFactory.logOut = _logOut;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.authentication = _authentication;
    authServiceFactory.checkLogin = _checkLogin;

    return authServiceFactory;
    }
})();