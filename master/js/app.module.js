/*!
 * 
 * Angle - Bootstrap Admin App + AngularJS
 * 
 * Version: 3.6
 * Author: @themicon_co
 * Website: http://themicon.co
 * License: https://wrapbootstrap.com/help/licenses
 * 
 */

// APP START
// ----------------------------------- 

(function() {
    'use strict';

    angular
        .module('angle', [
            'app.core',
            'app.routes',
            'app.sidebar',
            'app.navsearch',
            'app.preloader',
            'app.loadingbar',
            'app.translate',
            'app.settings',
            'app.dashboard',
            'app.icons',
            'app.flatdoc',
            'app.notify',
            'app.bootstrapui',
            'app.elements',
            'app.panels',
            'app.charts',
            'app.forms',
            'app.locale',
            'app.maps',
            'app.pages',
            'app.tables',
            'app.extras',
            'app.mailbox',
            'app.utils'
        ])
        .controller('mainCtrl', ['$scope', '$http', '$rootScope', 'authService','$cookies','$state', function ($scope, $http, $rootScope,authService,$cookies,$state) {

 }])
        .config(['$provide', '$routeProvider', '$httpProvider', function ($provide, $routeProvider, $httpProvider) {

    $httpProvider.interceptors.push('authInterceptorService');

}])
.run(['authService', '$rootScope', '$cookies','$state','$timeout', function (authService, $rootScope, $cookies,$state,$timeout) {
    authService.fillAuthData();
    $rootScope.$on("$stateChangeStart", function (event, next, current) {
        if (authService.authentication.isAuth) {
              if ($cookies.getObject('authorizationData').role != "admin") {
                if (next.access != undefined && !next.access.allowAnonymous) {
                      authService.logOut();
                    $timeout(function() {
                        $state.go('page.login');
                    });
                }
              }
        }
        if(!authService.authentication.isAuth)
        {
            if(next.name == 'page.recover')
            {
                $state.go("page.recover", {}, {notify:false});
            }
            else if(next.name != 'page.login')
            {
                $state.go("page.login");
                event.preventDefault();
            }
        }
    });
}])
.run(['$rootScope', '$location', '$injector', 'authService','$state', function ($rootScope, $location, $injector, authService,$state) {

    $rootScope.logout = function () {
        authService.logOut();
        $state.go("page.login");
    }

}])
.constant('rootUrl', 'http://ec2-54-148-33-87.us-west-2.compute.amazonaws.com:3005');
})();

